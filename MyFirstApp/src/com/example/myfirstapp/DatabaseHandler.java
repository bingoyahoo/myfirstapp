package com.example.myfirstapp;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {

	//all static variables 
	//Database Version
	private static final int DATABASE_VERSION = 6; //Need to increase version or clear cache if make changes, else app assume it is same

	//Database Name
	private static final String DATABASE_NAME = "crowdSizeManager";

	//crowd size table name
	private static final String TABLE_LOCATION = "locations";

	//Crowd Size Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_NAME = "name";
	private static final String KEY_CROWD_SIZE = "crowdSize";
	private static final String KEY_TIME = "sqlTime";

	public DatabaseHandler(Context context){
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	//Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db){
		String CREATE_CROWD_SIZE_TABLE  = "CREATE TABLE " + TABLE_LOCATION + "(" 
				+ KEY_ID + " INTEGER PRIMARY KEY,"
				+ KEY_NAME + " TEXT," 
				+ KEY_CROWD_SIZE + " TEXT,"
				+ KEY_TIME + " DATETIME DEFAULT CURRENT_TIMESTAMP"
				+ ")";
		db.execSQL(CREATE_CROWD_SIZE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion ){
		//Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION);

		//Create tables again
		onCreate(db);

	}

	//The following few methods are CRUD (Create, Read, Update and Delete) methods.
	//Adding new Location to database by building ContentValues using Location objects
	//Remember to close database after inserting data.
	public void addLocation(Location location){
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, location.getName());
		values.put(KEY_CROWD_SIZE, location.getCrowdSize());

		//Inserting row
		db.insert(TABLE_LOCATION, null, values);
		db.close(); //Closing database connection
	}
	//To read a row of Location from database from input id
	//Returns matched row from database
	public Location getLocation(int id){
		SQLiteDatabase db = this.getReadableDatabase(); //notice change to Readable

		Cursor cursor = db.query(TABLE_LOCATION, new String[] { KEY_ID,
				KEY_NAME, KEY_CROWD_SIZE, KEY_TIME }, KEY_ID + "=?",
				new String[] {String.valueOf(id)}, null, null, null , null);
		if (cursor != null) 
			cursor.moveToFirst();

		Location location = new Location(Integer.parseInt(cursor.getString(0)), 
				cursor.getString(1), cursor.getString(2));
		//return location
		return location;

	}
	
	//Getting all locations. Return all location from database in ArrayList format.
	//Requires a for loop after that to go through each contact.
	public List<Location> getAllLocations(){
		List<Location> locationList = new ArrayList<Location>();
		//Select all query
		String selectQuery = "SELECT * FROM " + TABLE_LOCATION;
		
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db. rawQuery(selectQuery, null); //rawQuery will return a cursor
		
		//Looping through all rows and adding each to the list
		if (cursor.moveToFirst()) { //If there is a first row aka not empty (I think)
			do {
				Location location = new Location();
				location.setID(Integer.parseInt(cursor.getString(0)));
				location.setName(cursor.getString(1));
				location.setCrowdSize(cursor.getString(2));
				location.setTime(cursor.getString(3));
				//Adding this location to list
				locationList.add(location);
			} while (cursor.moveToNext()); //moveToNext can check whether is past last entry already
		}
		return locationList;
	}
	//Returns total number of location entries in the database
	public int getLocationCount(){
		String countQuery = "SELECT * FROM "+ TABLE_LOCATION;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();
		
		//return count
		return cursor.getCount();
	}
	
	//Updating single location in database. Takes in a Location class object 
	//as parameter
	public int updateLocation(Location location) {
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(KEY_NAME,  location.getName());
		values.put(KEY_CROWD_SIZE,  location.getCrowdSize());
		values.put(KEY_TIME,  location.getTime());
		
		//Updating row
		return db.update(TABLE_LOCATION, values, KEY_ID + " = ?" , new String[] {String.valueOf(location.getID()) });
	}
	
	//Deleting single record from database
	public void deleteLocation(Location location){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_LOCATION, KEY_ID+ " = ?", new String[] { String.valueOf(location.getID())});
		db.close(); //Seems like only is return type is void need to close() explicitly.
	}
}
