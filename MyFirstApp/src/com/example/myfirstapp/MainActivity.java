package com.example.myfirstapp;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
	public final static String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
	static final int REQUEST_VIDEO_CAPTURE = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
			.add(R.id.container, new PlaceholderFragment()).commit();
		}
		DatabaseHandler db = new DatabaseHandler(this);

		/**
		 * CRUD Operations
		 * */
		// Inserting Locations
		Log.d("Insert: ", "Inserting .."); 
		db.addLocation(new Location("Engin Busstop", "33"));        
		db.addLocation(new Location("LT1", "200"));
		db.addLocation(new Location("LT6", "86"));
		db.addLocation(new Location("COM1", "123"));

		// Reading all locations
		Log.d("Reading: ", "Reading all locations.."); 
		List<Location> locations = db.getAllLocations();      

		for (Location lcn : locations) {
			String log = "Id: "+lcn.getID()+", Place: " + lcn.getName() + ", Crowd level: " + lcn.getCrowdSize() + ", Time: " + lcn.getTime();
			// Writing Locations to log
			Log.d("Name: ", log);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_activity_actions, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case R.id.action_search:
			openSearch();
			return true;
		case R.id.action_settings:
			openSettings();
			return true;
		case R.id.action_take_video:
			dispatchTakeVideoIntent();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}
	/** Called when the user clicks the Send button */
	public void sendMessage(View view) {
		Intent intent = new Intent(this, DisplayMessageActivity.class);
		EditText editText = (EditText) findViewById(R.id.edit_message);
		String message = editText.getText().toString();
		intent.putExtra(EXTRA_MESSAGE, message);
		startActivity(intent);
	}
	private void openSearch() {
		Toast.makeText(this, "Search button pressed", Toast.LENGTH_SHORT).show();
	}

	private void openSettings() {
		Toast.makeText(this, "Settings button pressed", Toast.LENGTH_SHORT).show();
	}

	//Here's a function that invokes an intent to capture video.
	private void dispatchTakeVideoIntent() {
		Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
		if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
			startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
		}
		Toast.makeText(this, "Take video from camera selected", Toast.LENGTH_SHORT).show();
	}

}
