package com.example.myfirstapp;

public class Location {
	private int id;
	private String name;
	private String crowdSize;
	private String time;

	//Constructors 
	public Location (){
	}
	
	public Location(int id, String name, String crowdSize){
	this.id = id;
	this.name = name;
	this.crowdSize = crowdSize;
	}
	
	public Location(String name, String crowdSize){
	this.name = name;
	this.crowdSize = crowdSize;
	}
	
	//Accessors
	//to get id
	public int getID(){
		return id;
	}
	//to get name
	public String getName(){
		return name;
	}
	//to get crowd size
	public String getCrowdSize(){
		return crowdSize;
	}
	//to get time
	public String getTime(){
		return time;
	}

	//Mutators
	//to set id
	public void setID(int id){
		this.id = id;
	}
	//to set name
	public void setName (String name){
		this.name = name;
	}
	//to set crowd size
	public void setCrowdSize (String size){
		this.crowdSize = size;
	}
	//to set crowd size
	public void setTime (String time){
		this.time = time;
	}
	
}
